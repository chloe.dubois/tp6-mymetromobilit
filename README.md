# MyMetromobilité

## Objectifs du projet

L'objectif est de créer un site internet responsive permettant de consulter les horaires de toutes les lignes des transports en commun de l’agglomération Grenobloise. Pour cela, nous utilisons les données de l'API de la Metro : https://www.mobilites-m.fr/pages/opendata/OpenDataApi.html

## Contenu du dossier

Le projet se compose d'une page html (index.html), dont le contenu évolue dynamiquement, d'un fichier js (dans un dossier scripts), et d'un dossier assets (comprenant les images et les feuilles de style). Le style est réalisé avec sass (fichier style-dev.scss, compilé en style.css).

## Installation

Pour utiliser le projet, installer les dépendances avec npm (jQuery et Leaflet), ouvrir index.html dans le navigateur.

## Recettage du site

Le site a été recetté sur gitlab dans les Issues (1 issue).







