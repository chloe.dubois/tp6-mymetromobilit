const myMap = L.map('myMap').setView([45.191816, 5.721656], 12);

$(document).ready(() => {
// fonction affichage des données LIGNES dans le DOM
  function affichageLignes(data, transporteur, type) {
    $('#listeLigne').html(''); // vide le container
    $.each(data, (index, value) => {
      if (value.id.match(transporteur)) {
        if (value.type === type) {
        // création d'une div et ajout d'un id (= id de la ligne) et d'une classe
          const ligneDiv = $('<div></div>').addClass('ligneDiv').attr('id', value.id);

          // Mode (icone) et type de transport
          const ligneType = $('<div></div>').html(`<p>${value.type}</p>`).addClass('ligneType');
          const ligneMode = $('<img>').css('margin-right', '6px');
          if (value.mode === 'BUS') {
            ligneMode.attr({ src: './assets/images/bus-dark.svg', alt: 'bus' });
          } else if (value.mode === 'TRAM') {
            ligneMode.attr({ src: './assets/images/tram-dark.svg', alt: 'tram' });
          } else if (value.mode === 'RAIL') {
            ligneMode.attr({ src: './assets/images/train-dark.svg', alt: 'train' });
          }
          $(ligneType).prepend(ligneMode);

          // Nom de la ligne
          const ligneName = $('<div></div>').addClass('ligneName');
          const ligneShortName = $('<p></p>').text(value.shortName).addClass('ligneShortName');
          if (value.color === '') {
            $(ligneShortName).css('background-color', 'rgb(228, 228, 228)'); // si pas de couleur, gris très pale
          } else {
            $(ligneShortName).css({ color: `#${value.textColor}`, 'background-color': `#${value.color}` });
          }
          if (value.type === 'TRAM' || value.type === 'CHRONO') {
            $(ligneShortName).addClass('rond');
          }
          const ligneLongName = $('<p></p>').text(value.longName).addClass('ligneLongName');
          $(ligneName).append(ligneShortName, ligneLongName);

          // Boutons
          const boutonsDiv = $('<div></div>').addClass('boutonsDiv');
          const boutonDetail = $('<a></a>').text('Afficher les détails').addClass('boutonDetail').attr({ 'data-id': value.id, 'data-type': value.type });
          const boutonFavori = $('<a></a>').text('Ajouter aux favoris').addClass('addFavori').attr({ 'data-id': value.id, 'data-shortname': value.shortName });
          $(boutonsDiv).append(boutonDetail, boutonFavori);

          $(ligneDiv).append(ligneType, ligneName, boutonsDiv);
          $('#listeLigne').append(ligneDiv);
        }
      }
    });
  }

  // alert

  function alertCovid() {
    $('#alertCovid').slideDown();
  }
  setTimeout(alertCovid, 2000); // fait apparaitre l'alert 2sec après chargement du DOM

  $('#close-alert').click(() => { // fait disparaitre l'alert au click et réapparaitre après 10 sec
    $('#alertCovid').slideUp();
    setTimeout(alertCovid, 60000);
  });

  // NAVIGATION
  // on cache le sousmenu
  $('.navigation ul.subMenu').hide();

  // on clique sur la barre
  $('.navigation li.toggleSubMenu > a').click(function () {
    // si menu open, on le ferme
    if ($(this).next('ul.subMenu:visible').length !== 0) {
      $(this).next('ul.subMenu').slideUp('normal', function () { $(this).parent().removeClass('open'); });
    // si menu fermé, on ferme les autres et on l'ouvre
    } else {
      $('.navigation ul.subMenu').slideUp('normal', function () { $(this).parent().removeClass('open'); });
      $(this).next('ul.subMenu').slideDown('normal', function () { $(this).parent().addClass('open'); });
    }
  });

  // on clique sur un lien vers les données
  $('.navigation ul.subMenu li > a').click(() => {
    // on ferme tous les menus
    $('.navigation ul.subMenu').slideUp('normal', function () { $(this).parent().removeClass('open'); });
    // affichage du bon container
    $('#listeLigne').fadeIn();
    $('#ligneDetails').fadeOut();
    $('#welcome').hide();
  });
  // pour le bouton FAVORIS voir plus bas

  const urlLignes = 'http://data.metromobilite.fr/api/routers/default/index/routes';
  // les transporteurs
  const semitag = /^SEM/;
  const transisere = /^C38/;
  const touGo = /^GSV/;
  const sncf = /^SNC/;
  const paysVoiron = /^TPV/;

  // les types de transports
  const tram = 'TRAM';
  const chrono = 'CHRONO';
  const flexo = 'FLEXO';
  const proximo = 'PROXIMO';
  const scolaires = 'SCOL';
  const structurantes = 'Structurantes';
  const secondaires = 'Secondaires';
  const urbaines = 'Urbaines';
  const interurbaines = 'Interurbaines';
  const tad = 'TAD';
  const c38 = 'C38';
  const snc = 'SNC';

  $.ajax({
    dataType: 'json',
    url: urlLignes,
  }).done((result) => {
    $('#getTram').click(() => {
      affichageLignes(result, semitag, tram);
    });
    $('#getChrono').click(() => {
      affichageLignes(result, semitag, chrono);
    });
    $('#getProximo').click(() => {
      affichageLignes(result, semitag, proximo);
    });
    $('#getFlexo').click(() => {
      affichageLignes(result, semitag, flexo);
    });
    $('#getC38').click(() => {
      affichageLignes(result, transisere, c38);
    });
    $('#getTransScol').click(() => {
      affichageLignes(result, transisere, scolaires);
    });
    $('#getStruct').click(() => {
      affichageLignes(result, touGo, structurantes);
    });
    $('#getSecond').click(() => {
      affichageLignes(result, touGo, secondaires);
    });
    $('#getTougScol').click(() => {
      affichageLignes(result, touGo, scolaires);
    });
    $('#getUrba').click(() => {
      affichageLignes(result, paysVoiron, urbaines);
    });
    $('#getTransurb').click(() => {
      affichageLignes(result, paysVoiron, interurbaines);
    });
    $('#getTad').click(() => {
      affichageLignes(result, paysVoiron, tad);
    });
    $('#getVoirScol').click(() => {
      affichageLignes(result, paysVoiron, scolaires);
    });
    $('#getSnc').click(() => {
      affichageLignes(result, sncf, snc);
    });
  });

  // LES FAVORIS

  // Ajouter un favori
  // j'enregistre l'identifiant de la ligne dans le localStorage

  function notInlSt(identifiant) { // regarde si un id est dans le local Storage (return true si non)
    let occurence = 0;
    for (let i = 0; i < localStorage.length; i += 1) {
      // Les datas du local storage : clef + valeur
      const key = localStorage.key(i);
      const value = localStorage.getItem(key);
      if (value === identifiant) {
        occurence =+ 1;
      }
    }
    if (occurence === 0) {
      return true;
    }
  }

  $('#listeLigne').on('click', '.addFavori', (e) => {
    const itemNotInlocalSt = notInlSt($(e.target).attr('data-id'));
    if (itemNotInlocalSt) {
      const nb = Date.now();
      localStorage.setItem(`favori ${nb}`, $(e.target).attr('data-id'));
      swal({
        title: $(e.target).attr('data-shortname'),
        text: 'a été ajouté aux favoris',
        icon: './assets/images/bigstar.svg',
        className: 'sweetalert',
        buttons: false,
        timer: 2000,
      });
    } else {
      swal({
        title: $(e.target).attr('data-shortname'),
        text: 'est déjà dans les favoris',
        icon: './assets/images/bigstar.svg',
        className: 'sweetalert',
        buttons: false,
        timer: 2000,
      });
    }
  });

  // Supprimer un favori
  $('#listeLigne').on('click', '.deleteFavori', (e) => {
    localStorage.removeItem($(e.target).attr('data-favkey')); // efface du localStorage
    $(e.target).parents('.ligneDiv').remove(); // efface le bloc affiché

    if (localStorage.length === 0) {
      $('#noFav').fadeIn(); // affiche le message d'alerte 'pas de favoris'
    }
  });

  // je créé un tableau pour récupérer les données du localStorage
  let favoris = [];

  function getItemsStorage() {
    for (let i = 0; i < localStorage.length; i += 1) {
      // Les datas du local storage : clef + valeur
      const key = localStorage.key(i);
      const value = localStorage.getItem(key);

      // Je créé un objet pour chaque favori
      const favItem = {};
      favItem.favNb = key;
      favItem.ligneId = value;

      // J'envoie l'objet dans mon tableau
      favoris.push(favItem);
    }
  }

  // fonction affichage des données FAVORIS dans le DOM
  function affichageFav(data, key) {
  // création d'une div et ajout d'un id (= id de la ligne) et d'une classe
    const ligneDiv = $('<div></div>').addClass('ligneDiv').attr('id', data[0].id);
    // Mode (icone) et type de transport
    const ligneType = $('<div></div>').html(`<p>${data[0].type}</p>`).addClass('ligneType');
    const ligneMode = $('<img>').css('margin-right', '6px');
    if (data[0].mode === 'BUS') {
      ligneMode.attr({ src: './assets/images/bus-dark.svg', alt: 'bus' });
    } else if (data[0].mode === 'TRAM') {
      ligneMode.attr({ src: './assets/images/tram-dark.svg', alt: 'tram' });
    } else if (data[0].mode === 'RAIL') {
      ligneMode.attr({ src: './assets/images/train-dark.svg', alt: 'train' });
    }
    $(ligneType).prepend(ligneMode);

    // Nom de la ligne
    const ligneName = $('<div></div>').addClass('ligneName');
    const ligneShortName = $('<p></p>').text(data[0].shortName).addClass('ligneShortName');
    if (data[0].color === '') {
      $(ligneShortName).css('background-color', 'rgb(228, 228, 228)'); // si pas de couleur, gris très pale
    } else {
      $(ligneShortName).css({ color: `#${data[0].textColor}`, 'background-color': `#${data[0].color}` });
    }
    if (data[0].type === 'TRAM' || data[0].type === 'CHRONO') {
      $(ligneShortName).addClass('rond');
    }
    const ligneLongName = $('<p></p>').text(data[0].longName).addClass('ligneLongName');
    $(ligneName).append(ligneShortName, ligneLongName);

    // Boutons
    const boutonsDiv = $('<div></div>').addClass('boutonsDiv');
    const boutonDetail = $('<a></a>').text('Afficher les détails').addClass('boutonDetail').attr({ 'data-id': data[0].id, 'data-type': data[0].type });
    const boutonFavori = $('<a></a>').text('Supprimer le favori').addClass('deleteFavori').attr('data-favkey', key);
    $(boutonsDiv).append(boutonDetail, boutonFavori);

    $(ligneDiv).append(ligneType, ligneName, boutonsDiv);
    $('#listeLigne').append(ligneDiv);
  }

  $('#fav').on('click', () => {
    $('#ligneDetails').fadeOut();
    $('#welcome').hide();
    $('#listeLigne').fadeIn();
    $('.navigation ul.subMenu').slideUp('normal', function () { $(this).parent().removeClass('open'); });
    $('#listeLigne').html(''); // vide le container
    const noFav = $('<p></p>').text('Vous n\'avez pas de favoris.').attr('id', 'noFav');
    $('#listeLigne').append(noFav);
    noFav.hide();
    favoris = []; // vide l'array
    getItemsStorage();
    if (localStorage.length) {
      const urlFavoris = 'http://data.metromobilite.fr/api/routers/default/index/routes?codes='; // + id de la ligne
      $.each(favoris, (index, value) => {
        const idStorage = value.ligneId;
        const keyStorage = value.favNb;
        $.ajax({
          dataType: 'json',
          url: urlFavoris + idStorage,
        }).done((result) => {
          affichageFav(result, keyStorage);
        });
      });
    } else {
      $(noFav).show();
    }
  });

  // DÉTAILS : HORAIRES & MAP
  // fonction affichage des horaires si disponibles

  function affichageHoraires(arretsArray) {
    const tableau = $('<table></table>').addClass('tableauHoraires');
    $('#horairesDiv').append(tableau);
    const caption = $('<caption></caption>');
    const tbody = $('<tbody></tbody>');
    $(tableau).append(caption, tbody);

    // le titre du tableau indique le terminus = dernier item de l'array
    $(caption).text(`Direction ${arretsArray[arretsArray.length - 1].stopName}`);

    function heures(trip) {
      let monHoraire = '';
      if (trip === '|') {
        monHoraire = '—';
      } else {
        const date = new Date();
        date.setUTCHours(0);
        date.setUTCMinutes(0);
        date.setUTCSeconds(0);
        const d = new Date(date.getTime() + (trip * 1000));
        monHoraire = `${d.getHours()}h${String(d.getMinutes()).padStart(2, '0')}`;
      }
      return monHoraire;
    }

    $.each(arretsArray, (index, value) => {
      const ArretsRow = $('<tr></tr>');
      const ArretsName = $('<td></td>').text(value.stopName);
      const ArretsTime1 = $('<td></td>').css('text-align', 'center').text(heures(arretsArray[index].trips[0]));
      const ArretsTime2 = $('<td></td>').css('text-align', 'center').text(heures(arretsArray[index].trips[1]));
      const ArretsTime3 = $('<td></td>').css('text-align', 'center').text(heures(arretsArray[index].trips[2]));
      const ArretsTime4 = $('<td></td>').css('text-align', 'center').text(heures(arretsArray[index].trips[3]));

      $(ArretsRow).append(ArretsName, ArretsTime1, ArretsTime2, ArretsTime3, ArretsTime4);
      $(tbody).append(ArretsRow);
    });
  }

  // Acceder aux détails au click du bouton "afficher les horaires"
  const urlHoraires = 'http://data.metromobilite.fr/api/ficheHoraires/json?route='; // ajouter id de la ligne
  const urlDetails = 'http://data.metromobilite.fr/api/lines/json?types=ligne&codes='; // ajouter id de la ligne

  $('#listeLigne').on('click', '.boutonDetail', (e) => {
    const identifiantLigne = $(e.target).attr('data-id');
    $('#listeLigne').fadeOut();
    $('#ligneDetails').fadeIn();
    $('#horairesDiv').empty();
    $('#noData').hide();

    $.ajax({
      dataType: 'json',
      url: urlDetails + identifiantLigne,
    }).done((result) => {
    // affichage du nom de la ligne en titre (+ numéro & couleur)
      const details = result.features[0].properties;
      $('#iconeLigne').text(details.NUMERO).css({ 'background-color': `rgb(${details.COULEUR})`, color: `rgb(${details.COULEUR_TEXTE})` });
      const type = $(e.target).attr('data-type');
      if (type === 'TRAM' || type === 'CHRONO') {
        $('#iconeLigne').addClass('round');
      } else {
        $('#iconeLigne').removeClass('round');
      }
      $('#nomLigne').text(details.LIBELLE);

      // MAP
      L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png').addTo(myMap);
      const geoJsonLine = result;
      const myColor = `rgb(${details.COULEUR})`;
      L.geoJSON(geoJsonLine, {
        style: {
          color: myColor,
        },
      }).addTo(myMap);
    });

    $.ajax({
      dataType: 'json',
      url: urlHoraires + identifiantLigne,
    }).done((result) => {
      if (result[0].arrets.length === 0) {
        $('#noData').show();
      } else {
        $('#noData').hide();
        affichageHoraires(result[0].arrets);
      }

      if (result[1].arrets.length === 0) {
        $('#noData').show();
        $('#myMap').hide();
      } else {
        $('#noData').hide();
        $('#myMap').show();
        affichageHoraires(result[1].arrets);
      }
    });
  });
}); // fin document ready
